import { useState , useCallback} from "react";
import Form from "react-bootstrap/Form";
import {useNavigate} from "react-router-dom"


function Create(props){
    
    const [entry_uname,setUname]=useState("");
    const [entry_pass,setPass]=useState("");
    const [createError,setCreateError]=useState("");
    const navigate = useNavigate();
    const navigateLogin  = useCallback(() => navigate('/bemoody/login'), [navigate]);

  const handleSubmit = (event)=>{
    let unameExist = props.database.find((entry) => entry.username === entry_uname);
    if (!unameExist){
    setCreateError("")
    let new_entry={};
    new_entry["username"]=entry_uname.toString();
    new_entry["password"]=entry_pass.toString();
    const database = props.database.map(a => {return {...a}});
    database.push(new_entry);
    props.setDatabase(database);
    //
    navigate('/bemoody/login')

    }else{
      setCreateError("Username already taken!")
      event.preventDefault();
    }

    
  }

  function validateForm(){
      return (entry_uname==="") || (entry_pass === "")
  }

  return(
      <div className="App">
          <div className="App-header"><h1>Welcome to the Smart Chindogu Belt </h1></div>
        <b/>
        <h3>Create User:</h3>
        <Form onSubmit={handleSubmit}>
          <div className="myInput">
            <Form.Group controlId="Username">
              <Form.Label>Set Username:</Form.Label>
              <Form.Control
                autoFocus
                type="Username"
                value={entry_uname}
                onChange={(e) => setUname(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="Password">
              <Form.Label>Set Password:</Form.Label>
              <Form.Control
                type="Password"
                value={entry_pass}
                onChange={(e) => setPass(e.target.value)}
              />
            </Form.Group>
            {createError !== "" ? (<p>{" "}{createError}</p>) : ("")}
          </div>
          <button class="bn3637 bn37"  size="lg" type="create" disabled={validateForm()}>
            Create User
          </button>
        </Form>
        <button  class="bn3637 bn37" size="lg" type="create" onClick={navigateLogin} >
          Login
        </button>
      </div>
  );

}

export default Create