
import {React, useState, useEffect}  from 'react';
import Login from './Login';
import './App.css';
import Create from './Create';
import Dashboard from './Dashboard';
import PageNotFound from './PageNotFound';
import { Route, Routes, Navigate } from "react-router-dom";


export class User{
  username;
  password;

  constructor(uname,pass){
    this.username =uname;
    this.password = pass;

  };

  setUsername(uname){
    this.username=uname;
    return
  }
  setPassword(pass){
    this.password=pass;
    return
  }

}

function isPersistedLoginData(obj){
  return (
    obj.username &&
    obj.password

  );
}

export async function loadUser() {
  // initialize `User` from localstorage data if available
  const unparsed= localStorage.getItem("loginData");
  if (!unparsed) return;
  const parsed = JSON.parse(unparsed);
  if (!isPersistedLoginData(parsed)) return;
  const user = new User(parsed.username, parsed.password);
  return user;
}

export function saveUser(u){
  // save login data to localstorage
  let data = {
    username: u.username,
    password: u.password,
  };
  localStorage.setItem("loginData", JSON.stringify(data));
}

function App() {
  const [user, setUser] = useState(undefined);
  const [database,setDatabase] = useState([
    {
      username: "user1",
      password: "pass1"
    },
    {
      username: "user2",
      password: "pass2"
    }
  ]) ;

  useEffect(() => {
    loadUser().then((u)=>(u ? setUser(u):undefined));
  }, []);

  useEffect(() => (user ? saveUser(user):localStorage.removeItem("loginData")),[user]);

  if(!user){
    console.log("no User")
    return(
      <Routes>
        <Route exact path="/bemoody/" children={({database,setUser})=><Login database={database} setUser={setUser} />} element={<Login database={database} setUser={setUser} />}/>
        <Route exact path="/bemoody/login" childeren={({database,setUser})=><Login database={database} setUser={setUser}/>} element={<Login database={database} setUser={setUser}/>} />
        <Route exact path="/bemoody/create" children={({database,setDatabase})=><Create database={database} setDatabase={setDatabase}/>} element={<Create database={database} setDatabase={setDatabase}/>} />
        <Route path="*" children={()=><Navigate replace to="/bemoody/login" />} />
      </Routes>
    );
  } 
  if(user){
    console.log("USER!!!!!!")
     return(  <Routes>
          <Route exact path="/bemoody/dashboard" children={ ({user,setUser})=><Dashboard setUser={setUser} user={user} />} element={<Dashboard setUser={setUser} user={user} />} />
          <Route exact path="/bemoody/"  children={ ({user,setUser})=><Dashboard setUser={setUser} user={user} />} element={<Dashboard setUser={setUser} user={user} />} />
          <Route exact path="/bemoody/404" children={()=><PageNotFound/>} element={<PageNotFound/>}/>
        <Route path="*" children={()=><Navigate replace to="/bemoody/dashboard" />} element={<Navigate replace to="/bemoody/dashboard" />} />
        </Routes>
     );
  }

  return(

      <Routes>
        <Route exact path="/bemoody/dashboard" children={ ({user,setUser})=><Dashboard setUser={setUser} user={user} />} element={<Dashboard setUser={setUser} user={user} />} />
        <Route exact path="/bemoody/404" children={()=><PageNotFound/>} element={<PageNotFound/>}/>
        <Route path="*" children={()=><Navigate replace to="/404" />} element={<Navigate replace to="/bemoody/404" />} />
      </Routes>

  );
}

export default App;

