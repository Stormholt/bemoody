import GaugeChart from 'react-gauge-chart'
import uibuilder from 'node-red-contrib-uibuilder/front-end/src/uibuilderfe'
import {useNavigate} from 'react-router-dom'
import { useState, useEffect} from "react";




function Dashboard(props){

    uibuilder.start()

    const [posted, setPosted] = useState(false)
    const [nodeMsgRecv, setNodeMsgRecv] = useState(undefined);
    const [ you, setYou] = useState( {id:4200, name:"unknown", type:"undefined", value:0  });
    const [ friend1, setFriend1] = useState( {id:1234, name:"unknown", type:"undefined", value:0  })
    const [ friend2, setFriend2] = useState( {id:2345, name:"unknown", type:"undefined", value:0  })
    const [ friend3, setFriend3] = useState( {id:6969, name:"unknown", type:"undefined", value:0  })
    const [value, SetValue]= useState(0)
    const navigate = useNavigate();
   // let friend2 = {id:2345, name:"unknown", type:"undefined", value:0  }
    //let friend3 = {id:6969, name:"unknown", type:"undefined", value:0  }
    const chartstyle={
        width:300, 
        height: 100,
        display: 'flex', 
        alignItems: 'center',

      
    }

    uibuilder.onChange('msg', function(msg){
        console.info('[uibuilder.onChange] property msg changed!', msg)
        setNodeMsgRecv(msg);
    })

    useEffect(() => {
        if(nodeMsgRecv !==undefined){
            console.log("nodeMsgReceived")
        
            if (nodeMsgRecv['payload'].toString().includes("1234")){
                //friend1 = nodeMsgRecv['payload']
                setFriend1(JSON.parse(nodeMsgRecv['payload']))
                //setYou(JSON.parse(nodeMsgRecv['payload']))
            
                setPosted(true)
                
                console.log(nodeMsgRecv['payload'])

            }
            else if (nodeMsgRecv['payload'].toString().includes("2345")){
               // friend2 = nodeMsgRecv['payload']
                setFriend2(JSON.parse(nodeMsgRecv['payload']))
            }
            else if (nodeMsgRecv['payload'].toString().includes("6969")){
               // friend3 = nodeMsgRecv['payload']
               setFriend3(JSON.parse(nodeMsgRecv['payload']))
            }
            else if (nodeMsgRecv['topic'].toString().includes("Friends")){
                //friend1 = nodeMsgRecv['payload']
                let data = nodeMsgRecv['payload'].split(";")
                console.log(data)
                setYou({id:4200,name: data[0], type:data[1], value:data[2] })
                
                setPosted(true)

            }
        }
     
    },[nodeMsgRecv]);

    useEffect(() => {
        SetValue(you.value*0.01)
     
    },[you]);



    function handleLogout(){
        props.setUser(undefined).then(navigate("/bemoody/login"));
        
    }

    function renderMoods(){
        return(
        <div className='gauge'>

        <div >
            <div >
            <h2>{friend1.name}</h2>
            <GaugeChart  id="gauge-chart1" 
            style={chartstyle}
            nrOfLevels={6}
            arcWidth={0.3}
            animateDuration={6000}
            percent={friend1.value} 
            textColor={"#FFFFFF"}
            colors={["#FF0000","#00FF00" ]}
            margin={{top: 10, right: 50, bottom: 50, left: 50}}/>
            
            <h4>{friend1.type}</h4>
            <b/>
            <h2>{friend2.name}</h2>
            <GaugeChart  id="gauge-chart2" 
            style={chartstyle}
            nrOfLevels={6}
            animateDuration={6000}
            arcWidth={0.3}
            percent={friend2.value} 
            textColor={"#FFFFFF"}
            colors={["#FF0000","#00FF00" ]}
            />
            <h4>{friend2.type}</h4>
            <b/>
            <h2>{friend3.name}</h2>
            <GaugeChart  id="gauge-chart3" 
            style={chartstyle}
            nrOfLevels={6}
            arcWidth={0.3}
            animateDuration={6000}
            percent={friend3.value} 
            textColor={"#FFFFFF"}
            colors={["#FF0000","#00FF00" ]}
            />
            <h4>{friend3.type}</h4>
            </div>
        </div>
        <div >
        <h2>You</h2>
            <GaugeChart id="gauge-chart4" 
            style={chartstyle}
            nrOfLevels={6}
            arcWidth={0.3}
            animateDuration={6000}
            percent={value} 
            textColor={"#FFFFFF"}
            colors={["#FF0000","#00FF00"]}
            />
            <h4>{you.type}</h4>
        </div>
    </div>)
    }
    
    return(
        <div className='App-dashboard'>
            <div className="App-header"><h1>Mood o' Meter Dashboard of  { props.user===undefined ? ("") : props.user.username } </h1></div> {/* TODOO: USER NAME */} 
            <b/>
            {posted ? renderMoods() : <p>Post your mood to see the dashboard</p>}
           

            <button class="bn3637 bn37" onClick={handleLogout} > Logout</button>
        </div>
        
    );
}

export default Dashboard